package az.ingress.lesson1.exception;

import az.ingress.lesson1.exception.custom.AlreadyExistsException;
import az.ingress.lesson1.exception.custom.IllegalArgumentException;
import az.ingress.lesson1.exception.custom.NotFoundException;
import az.ingress.lesson1.service.TranslateService;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.time.OffsetDateTime;
import java.util.Map;
import java.util.stream.Collectors;


@RestControllerAdvice
@RequiredArgsConstructor
public class ApplicationExceptionHandler {
    private final TranslateService translateService;

    @ExceptionHandler(AlreadyExistsException.class)
    public ResponseEntity<ErrorResponseDto> handleRuntimeException(AlreadyExistsException ex, WebRequest request) {
        String language = LocaleContextHolder.getLocale().getLanguage();

        String translatedMessage = translateService.translate(
                ex.getResponseCode().getDescription(),
                language,
                ex.getArgs()
        );
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ErrorResponseDto.builder()
                        .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        .code(ex.getResponseCode())
                        .message(translatedMessage)
                        .path(((ServletWebRequest) request).getRequest().getRequestURI())
                        .requestedLang(language)
                        .build());
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorResponseDto> handleRuntimeException(NotFoundException ex, WebRequest request) {
        String language = LocaleContextHolder.getLocale().getLanguage();
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(ErrorResponseDto.builder()
                        .status(HttpStatus.NOT_FOUND.value())
                        .code(ex.getResponseCode())
                        .message(ex.getMessage())
                        .path(((ServletWebRequest) request).getRequest().getRequestURI())
                        .requestedLang(language)
                        .build());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponseDto> handleRuntimeException(IllegalArgumentException ex, WebRequest request) {
        String language = LocaleContextHolder.getLocale().getLanguage();

        String translatedMessage = translateService.translate(
                ex.getResponseCode().getDescription(),
                language,
                ex.getArgs()
        );

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(ErrorResponseDto.builder()
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .code(ex.getResponseCode())
                        .message(translatedMessage)
                        .path(((ServletWebRequest) request).getRequest().getRequestURI())
                        .requestedLang(language)
                        .build());
    }


    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorResponseDto> handleRuntimeException(RuntimeException ex, WebRequest request) {
        String language = LocaleContextHolder.getLocale().getLanguage();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ErrorResponseDto.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .code(null)
                .message(ex.getMessage())
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .requestedLang(language)
                .build());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public final ResponseEntity<ErrorResponseDto> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex, WebRequest request) {
        String language = LocaleContextHolder.getLocale().getLanguage();

        Throwable cause = ex.getCause();
        String detailMessage = "Invalid JSON format";
        if (cause instanceof UnrecognizedPropertyException) {
            String invalidField = ((UnrecognizedPropertyException) cause).getPropertyName();
            detailMessage = String.format("Unrecognized field '%s' in request", invalidField);
        }

        ErrorResponseDto response = ErrorResponseDto.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .code(null)
                .message("Invalid request format")
                .detail(detailMessage)
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .requestedLang(language)
                .timestamp(OffsetDateTime.now())
                .build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public final ResponseEntity<ErrorResponseDto> handle(MethodArgumentNotValidException ex, WebRequest request) {
        String language = LocaleContextHolder.getLocale().getLanguage();

        Map<String, String> validationErrors = ex.getBindingResult().getFieldErrors().stream()
                .collect(Collectors.toMap(
                        FieldError::getField,
                        fieldError -> translateService.translate(fieldError.getDefaultMessage(), language),
                        (existing, replacement) -> existing));


        ErrorResponseDto response = ErrorResponseDto.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .code(null)
                .message(translateService.translate("VALIDATION_FAILED_MESSAGE", language, (String) null))
                .detail(translateService.translate("VALIDATION_FAILED_DETAIL", language, (String) null))
                .data(validationErrors)
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .requestedLang(language)
                .timestamp(OffsetDateTime.now())
                .build();

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(response);
    }
}
