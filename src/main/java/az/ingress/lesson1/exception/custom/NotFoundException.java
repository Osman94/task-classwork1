package az.ingress.lesson1.exception.custom;

import az.ingress.lesson1.exception.ResponseCode;

public class NotFoundException extends CustomException {

    public NotFoundException(ResponseCode responseCode, Object... args) {
        super(responseCode, args);
    }

    public NotFoundException(ResponseCode responseCode) {
        super(responseCode);
    }
}