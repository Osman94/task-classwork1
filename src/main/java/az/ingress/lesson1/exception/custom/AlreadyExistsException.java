package az.ingress.lesson1.exception.custom;

import az.ingress.lesson1.exception.ResponseCode;

public class AlreadyExistsException extends CustomException {

    public AlreadyExistsException(ResponseCode responseCode, Object parameter) {
        super(responseCode, parameter);
    }

    public AlreadyExistsException(ResponseCode responseCode) {
        super(responseCode);
    }
}