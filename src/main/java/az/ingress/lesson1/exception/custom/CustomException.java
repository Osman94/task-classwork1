package az.ingress.lesson1.exception.custom;

import az.ingress.lesson1.exception.ResponseCode;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Builder
@Slf4j
public class CustomException extends RuntimeException {
    private ResponseCode responseCode;
    private Object[] args;

    public CustomException(ResponseCode responseCode, Object... args) {
        super(responseCode.getDescription(args));
        this.responseCode = responseCode;
        this.args = args;  // Store args for translation
        log.info("Description: {}", responseCode.getDescription(args));
    }

    public CustomException(ResponseCode responseCode) {
        super(responseCode.getDescription());
        this.responseCode = responseCode;
    }
}