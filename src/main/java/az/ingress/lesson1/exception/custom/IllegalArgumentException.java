package az.ingress.lesson1.exception.custom;

import az.ingress.lesson1.exception.ResponseCode;

public class IllegalArgumentException extends CustomException {

    public IllegalArgumentException(ResponseCode responseCode, Object... args) {
        super(responseCode, args);
    }
    public IllegalArgumentException(ResponseCode responseCode) {
        super(responseCode);
    }
}