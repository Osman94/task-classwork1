package az.ingress.lesson1.exception.custom;

import az.ingress.lesson1.exception.ResponseCode;

public class InvalidTokenException extends CustomException {

    public InvalidTokenException(ResponseCode responseCode, Object... args) {
        super(responseCode, args);
    }

    public InvalidTokenException(ResponseCode responseCode) {
        super(responseCode);
    }
}