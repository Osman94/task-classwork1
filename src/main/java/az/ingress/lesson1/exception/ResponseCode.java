package az.ingress.lesson1.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseCode {

    USER_NOT_FOUND(10, "USER_NOT_FOUND"),
    USER_ALREADY_EXISTS(11, "USER_ALREADY_EXISTS"),
    INVALID_LOGIN_CREDENTIALS(12, "INVALID_LOGIN_CREDENTIALS"),
    INVALID_REFRESH_TOKEN(13, "INVALID_REFRESH_TOKEN"),
    ACCESS_TOKEN_EXPIRED(14, "ACCESS_TOKEN_EXPIRED"),
    INVALID_TOKEN(15, "INVALID_TOKEN"),

    CART_NOT_FOUND(2, "CART_NOT_FOUND"),
    PRODUCT_NOT_FOUND(3, "PRODUCT_NOT_FOUND"),
    CATEGORY_NOT_FOUND(4, "CATEGORY_NOT_FOUND"),
    CART_ALREADY_EXISTS(6, "CART_ALREADY_EXISTS"),
    PRODUCT_ALREADY_EXISTS(7, "PRODUCT_ALREADY_EXISTS"),
    CATEGORY_ALREADY_EXISTS(8, "CATEGORY_ALREADY_EXISTS"),
    INVALID_REQUEST(9, "INVALID_REQUEST")
    ;

    private final int code;
    private final String description;

    public String getDescription(Object... args) {
        return String.format(description, args);
    }

}