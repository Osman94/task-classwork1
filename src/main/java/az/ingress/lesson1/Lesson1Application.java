package az.ingress.lesson1;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import java.util.Locale;


@SpringBootApplication
@Slf4j
@AllArgsConstructor
@EnableCaching
@EnableWebSecurity
public class Lesson1Application implements CommandLineRunner {


	public static void main(String[] args) {
		SpringApplication.run(Lesson1Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Example");
	}
}
