package az.ingress.lesson1.service;

public class Calculator {

    public int add(int a, int b) {
        return a + b;
    }

    public int divide(int a, int b) {
        if(b == 0){
            throw new ArithmeticException("Divide by zero prohibited");
        }
        return a / b;
    }
}

