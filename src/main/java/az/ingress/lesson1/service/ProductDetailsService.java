package az.ingress.lesson1.service;

import az.ingress.lesson1.model.dto.ProductDetailsDto;

public interface ProductDetailsService {
    void createProductDetails(ProductDetailsDto productDetailsDto);
}
