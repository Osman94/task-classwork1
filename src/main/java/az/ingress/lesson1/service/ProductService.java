package az.ingress.lesson1.service;

import az.ingress.lesson1.model.projection.CategoryCountProjection;
import az.ingress.lesson1.model.dto.ProductDto;

import java.util.List;

public interface ProductService {
    void createProduct(ProductDto productDto);
    List<ProductDto> getProducts(Integer low , Integer high);
    List<CategoryCountProjection> getCategoryCounts();
    void deleteProduct(Integer id);
}
