package az.ingress.lesson1.service;

import az.ingress.lesson1.model.dto.CategoryDto;

import java.util.List;

public interface CategoryService {
    void createCategory(CategoryDto categoryDto);

    List<CategoryDto> getAllCategories();

    CategoryDto getCategory(Long id);
}
