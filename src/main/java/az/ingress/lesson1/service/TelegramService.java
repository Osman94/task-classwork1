package az.ingress.lesson1.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class TelegramService {

    @Value("${telegram.bot.token}")
    private String botToken;

    @Value("${telegram.chat.id}")
    private String chatId;

    public void sendMessage(String message) {
        String url = String.format("https://api.telegram.org/bot%s/sendMessage", botToken);

        WebClient.create(url)
                .post()
                .uri(uriBuilder -> uriBuilder.queryParam("chat_id", chatId)
                        .queryParam("text", message)
                        .queryParam("parse_mode", "Markdown") // For link formatting
                        .build())
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }
}
