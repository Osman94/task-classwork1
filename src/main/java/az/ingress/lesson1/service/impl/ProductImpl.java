package az.ingress.lesson1.service.impl;

import az.ingress.lesson1.model.projection.CategoryCountProjection;
import az.ingress.lesson1.model.dto.CategoryDto;
import az.ingress.lesson1.model.dto.ProductDetailsDto;
import az.ingress.lesson1.model.dto.ProductDto;
import az.ingress.lesson1.model.entity.Category;
import az.ingress.lesson1.model.entity.Product;
import az.ingress.lesson1.model.entity.ProductDetails;
import az.ingress.lesson1.repository.CategoryRepository;
import az.ingress.lesson1.repository.ProductDetailsRepository;
import az.ingress.lesson1.repository.ProductRepository;
import az.ingress.lesson1.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;


@Service
@RequiredArgsConstructor
@Slf4j
public class ProductImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ProductDetailsRepository productDetailsRepository;


    @Override
    public void createProduct(ProductDto productDto) {

        if (productRepository.existsByName(productDto.getName())) {
            throw new RuntimeException("Product name already exists");
        }

        ProductDetails productDetails = ProductDetails.builder()
                .image_url(productDto.getProductDetailsDto().getImage_url())
                .color(productDto.getProductDetailsDto().getColor())
                .build();

        Category category = categoryRepository.findByName(productDto.getCategory().getName())
                .orElseThrow(() -> new RuntimeException("Category not found"));

        Product product = Product.builder()
                .name(productDto.getName())
                .description(productDto.getDescription())
                .price(productDto.getPrice())
                .category(category)
                .productDetails(productDetails)
                .build();


        productRepository.save(product);
    }

    @Override
    public List<ProductDto> getProducts(Integer low, Integer high) {
        List<Product> products;
        if (low == null && high == null) {
            products = productRepository.findAll();
        } else {
            products = productRepository.findAllByPriceLessThanEqualAndPriceGreaterThanEqual(high, low);
        }
        List<ProductDto> productDtoList = new ArrayList<>();
        for (Product product : products) {
            ProductDetailsDto productDetailsDto = ProductDetailsDto.builder()
                    .image_url(product.getProductDetails().getImage_url())
                    .color(product.getProductDetails().getColor())
                    .build();

            CategoryDto categoryDto = CategoryDto.builder()
                    .name(product.getCategory().getName())
                    .build();

            ProductDto productDto = ProductDto.builder()
                    .name(product.getName())
                    .price(product.getPrice())
                    .description(product.getDescription())
                    .productDetailsDto(productDetailsDto)
                    .category(categoryDto)
                    .build();
            productDtoList.add(productDto);
        }
        return productDtoList;
    }

    public List<CategoryCountProjection> getCategoryCounts() {
        return productRepository.findCategoryCounts();
    }

    @Override
    public void deleteProduct(Integer id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Product not found with id :" + id));
        productRepository.delete(product);
    }
}
