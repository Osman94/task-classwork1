package az.ingress.lesson1.service.impl;

import az.ingress.lesson1.model.dto.ProductDetailsDto;
import az.ingress.lesson1.model.entity.ProductDetails;
import az.ingress.lesson1.repository.ProductDetailsRepository;
import az.ingress.lesson1.service.ProductDetailsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
@Slf4j
public class ProductDetailsImpl implements ProductDetailsService {

    private final ProductDetailsRepository productDetailsRepository;

    @Override
    public void createProductDetails(ProductDetailsDto productDetailsDto) {
        ProductDetails productDetails = ProductDetails.builder()
                .color(productDetailsDto.getColor())
                .image_url(productDetailsDto.getImage_url())
                .build();
        productDetailsRepository.save(productDetails);
    }
}
