package az.ingress.lesson1.service.impl;

import az.ingress.lesson1.exception.custom.AlreadyExistsException;
import az.ingress.lesson1.exception.custom.NotFoundException;
import az.ingress.lesson1.model.dto.CartDto;
import az.ingress.lesson1.model.entity.Cart;
import az.ingress.lesson1.model.entity.Product;
import az.ingress.lesson1.repository.CartRepository;
import az.ingress.lesson1.repository.ProductRepository;
import az.ingress.lesson1.service.CartService;
import az.ingress.lesson1.model.projection.CartWithProductDetailsProjection;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

import static az.ingress.lesson1.exception.ResponseCode.*;

/*
Test Ssenarilər:
Create Shopping Cart
Add Product to Cart
Remove Product from Cart
Get Shopping Cart by ID
Testlərin bütün mümkün success və exception halları əhatə etdiyindən
 (update zamanı datanın update olduğundan və redis cache'dən silindiyindən,
 GET zamanı data cache'də olduğu zaman redis'
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class CartImpl implements CartService {

    private final CartRepository cartRepository;
    private final ProductRepository productRepository;
    private final RedisTemplate<Object, Object> redisTemplate;

    @Override
    public void createShoppingCart(CartDto cartDto) {
        if (cartRepository.existsByName(cartDto.getName())) {
            throw new AlreadyExistsException(CATEGORY_ALREADY_EXISTS);
        }

        Cart cart = Cart.builder()
                .name(cartDto.getName())
                .build();
        cartRepository.save(cart);

        redisTemplate.opsForValue().set("cart:" + cart.getId(), cart);
        log.info("Shopping cart saved to redis with id : {}", cart.getId());
    }

    @Override
    public void addProductToCart(Long cartId, Long productId) {
        if (!cartRepository.existsById(cartId)) {
            throw new AlreadyExistsException(CATEGORY_ALREADY_EXISTS);
        }

        Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new NotFoundException(CART_NOT_FOUND));

        Product product = productRepository.findById(Math.toIntExact(productId))
                .orElseThrow(() -> new NotFoundException(PRODUCT_NOT_FOUND));

        product.getCarts().add(cart);
        productRepository.save(product);

        // Update product list in Redis
        redisTemplate.opsForList().rightPush("cartProducts:" + cartId, product);
        log.info("Product added to cart in Redis with cart id : {}", cartId);
    }

    @Override
    public void deleteProductFromCart(Long cartId, Long productId) {
        Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new NotFoundException(CART_NOT_FOUND));

        Product product = productRepository.findById(Math.toIntExact(productId))
                .orElseThrow(() -> new NotFoundException(PRODUCT_NOT_FOUND));

        List<Cart> carts = new ArrayList<>(product.getCarts());

        carts.remove(cart);
        product.setCarts(carts);

        productRepository.save(product);

        redisTemplate.opsForList().remove("cartProducts:" + cartId, 1, product);
        redisTemplate.delete("cart:" + cartId);

        log.info("Product removed from cart and Redis with cart id : {}", cartId);
    }



    @Override
    public List<CartWithProductDetailsProjection> getCartAndProducts(Long cartId) {
        List<CartWithProductDetailsProjection> cartProducts = (List<CartWithProductDetailsProjection>) redisTemplate.opsForValue().get("cartProducts:" + cartId);

        if (cartProducts == null) {
            if (!cartRepository.existsById(cartId)) {
                throw new NotFoundException(CART_NOT_FOUND);
            }

            cartProducts = cartRepository.getCartAndProductsDetails(cartId);
            redisTemplate.opsForValue().set("cartProducts:" + cartId, cartProducts);
            log.info("Cart and products details fetched from DB and saved to Redis with cart id : {}", cartId);
        } else {
            log.info("Cart and products details fetched from Redis with cart id : {}", cartId);
        }

        return cartProducts;
    }
}
