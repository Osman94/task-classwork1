package az.ingress.lesson1.service.impl;

import az.ingress.lesson1.model.dto.CategoryDto;
import az.ingress.lesson1.model.entity.Category;
import az.ingress.lesson1.repository.CategoryRepository;
import az.ingress.lesson1.repository.ProductRepository;
import az.ingress.lesson1.service.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
@RequiredArgsConstructor
@Slf4j
public class CategoryImpl implements CategoryService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    @Override
    public void createCategory(CategoryDto categoryDto) {

        if (categoryRepository.existsByName(categoryDto.getName())) {
            throw new RuntimeException("Category name already exists");

        }
        log.info("Creating category: {}", categoryDto);
        Category category = Category.builder()
                .name(categoryDto.getName())
                .build();
        categoryRepository.save(category);
    }

    @Override
    public List<CategoryDto> getAllCategories() {
        List<Category> categoryList = categoryRepository.findAll();
        List<CategoryDto> categoryDtoList = new ArrayList<>();
        for (Category category : categoryList) {
            CategoryDto categoryDto = CategoryDto.builder()
                    .name(category.getName())
                    .build();
            categoryDtoList.add(categoryDto);
        }
        return categoryDtoList;
    }

    @Override
    public CategoryDto getCategory(Long id) {
        Category category = categoryRepository.findById(id).orElseThrow(() -> new RuntimeException("Category name doesn't exists"));
        return CategoryDto.builder().name(category.getName()).build();
    }
}
