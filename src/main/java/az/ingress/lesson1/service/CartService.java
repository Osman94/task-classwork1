package az.ingress.lesson1.service;

import az.ingress.lesson1.model.dto.CartDto;
import az.ingress.lesson1.model.projection.CartWithProductDetailsProjection;

import java.util.List;


public interface CartService {
    void createShoppingCart(CartDto cartDto);

    void addProductToCart(Long cartId, Long productId);

    void deleteProductFromCart(Long cartId, Long productId);

    List<CartWithProductDetailsProjection> getCartAndProducts(Long cartId);


}
