package az.ingress.lesson1.service;

import az.ingress.lesson1.auth.BaseJwtService;
import az.ingress.lesson1.exception.custom.AlreadyExistsException;
import az.ingress.lesson1.exception.custom.IllegalArgumentException;
import az.ingress.lesson1.exception.custom.NotFoundException;
import az.ingress.lesson1.mapper.UserMapper;
import az.ingress.lesson1.model.dto.auth.CreateUserRequest;
import az.ingress.lesson1.model.dto.auth.LoginRequest;
import az.ingress.lesson1.model.dto.auth.RefreshTokenRequest;
import az.ingress.lesson1.model.entity.User;
import az.ingress.lesson1.model.response.LoginResponse;
import az.ingress.lesson1.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static az.ingress.lesson1.exception.ResponseCode.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final BaseJwtService baseJwtService;
    private final UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new NotFoundException(USER_NOT_FOUND));
    }

    public void createUser(CreateUserRequest createUserRequest) {
        userRepository.findByUsername(createUserRequest.getUsername()).ifPresentOrElse(
                user -> {
                    throw new AlreadyExistsException(USER_ALREADY_EXISTS, createUserRequest.getUsername());
                },
                () -> {
                    var newUser = userMapper.toUser(createUserRequest);
                    newUser.setPassword(passwordEncoder.encode(createUserRequest.getPassword()));
                    userRepository.save(newUser);
                }
        );
    }

    public LoginResponse login(LoginRequest loginRequest) {
        User user = userRepository.findByUsername(loginRequest.getUsername())
                .orElseThrow(() -> new IllegalArgumentException(INVALID_LOGIN_CREDENTIALS, loginRequest.getUsername()));
        boolean matches = passwordEncoder.matches(loginRequest.getPassword(), user.getPassword());
        if (!matches) {
            throw new IllegalArgumentException(INVALID_LOGIN_CREDENTIALS, loginRequest.getPassword());
        } else {
            String accessToken = baseJwtService.createAccessToken(user);
            String refreshToken = baseJwtService.createRefreshToken(user);
            return LoginResponse.builder()
                    .accessToken(accessToken)
                    .refreshToken(refreshToken)
                    .build();
        }
    }

    public LoginResponse generateTokensWithGivenRefreshToken(RefreshTokenRequest refreshTokenRequest) {
        String refreshToken = refreshTokenRequest.getRefreshToken();
        String userId = baseJwtService.isValidRefreshToken(refreshToken);
        User user = userRepository.findById(Long.parseLong(userId)).orElseThrow(() -> new NotFoundException(USER_NOT_FOUND,userId));
        String newAccessToken = baseJwtService.createAccessToken(user);
        String newRefreshToken = baseJwtService.createRefreshToken(user);
        return LoginResponse.builder()
                .accessToken(newAccessToken)
                .refreshToken(newRefreshToken)
                .build();
    }

}
