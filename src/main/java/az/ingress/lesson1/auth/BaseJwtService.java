package az.ingress.lesson1.auth;

import az.ingress.lesson1.exception.ResponseCode;
import az.ingress.lesson1.exception.custom.InvalidTokenException;
import az.ingress.lesson1.model.entity.Authority;
import az.ingress.lesson1.model.entity.User;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

import static az.ingress.lesson1.exception.ResponseCode.INVALID_REFRESH_TOKEN;

@Slf4j
@Component
@RequiredArgsConstructor
public final class BaseJwtService {

    private SecretKey key;

    @Value("${spring.security.jwt-secret-key}")
    private String jwtSecretKey;

    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode(jwtSecretKey);
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Jws<Claims> parseToken(String token) {
        return Jwts.parser()
                .verifyWith(key)
                .build()
                .parseSignedClaims(token);
    }

    public String isValidRefreshToken(String refreshToken) {
        try {
            var claims = parseToken(refreshToken);
            String tokenType = claims.getHeader().get("tokenType").toString();
            if ("refresh".equals(tokenType)) {
                Date expiration = claims.getPayload().getExpiration();
                if (expiration != null && !expiration.before(new Date())) {
                    log.info("Claim expiration: {}", expiration);
                    return claims.getPayload().getSubject();
                }
            }
            throw new InvalidTokenException(INVALID_REFRESH_TOKEN);
        } catch (SignatureException e) {
            log.error("Invalid refresh token", e);
            throw new InvalidTokenException(INVALID_REFRESH_TOKEN);
        } catch (Exception e) {
            log.error("Error validating refresh token", e);
            throw new InvalidTokenException(INVALID_REFRESH_TOKEN);
        }
    }

    public String createAccessToken(User user) {
        return Jwts.builder()
                .subject(user.getId().toString())
                .issuedAt(new Date())
                .expiration(Date.from(Instant.now().plus(Duration.ofMinutes(1))))
                .header()
                .add("typ", "JWT")
                .add("tokenType", "access")
                .and()
                .claim("authority", user.getAuthorities().stream().map(Authority::getAuthority).toList())
                .signWith(key)
                .compact();
    }

    public String createRefreshToken(User user) {
        return Jwts.builder()
                .subject(user.getId().toString())
                .issuedAt(new Date())
                .expiration(Date.from(Instant.now().plus(Duration.ofMinutes(2))))
                .header()
                .add("typ", "JWT")
                .add("tokenType", "refresh")
                .and()
                .signWith(key)
                .compact();
    }

}
