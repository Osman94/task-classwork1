package az.ingress.lesson1.mapper;


import az.ingress.lesson1.model.dto.auth.CreateUserRequest;
import az.ingress.lesson1.model.entity.User;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface UserMapper {


    User toUser(CreateUserRequest userDto);

}
