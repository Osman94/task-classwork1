package az.ingress.lesson1.controller;

import az.ingress.lesson1.model.projection.CategoryCountProjection;
import az.ingress.lesson1.model.dto.ProductDto;
import az.ingress.lesson1.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;


    @PostMapping
    public ResponseEntity<Void> createProduct(@RequestBody ProductDto productDto) {
        productService.createProduct(productDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Integer id) {
        productService.deleteProduct(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping
    public ResponseEntity<List<ProductDto>> getProducts(@RequestParam(required = false) Integer low, @RequestParam(required = false) Integer high) {
        List<ProductDto> products = productService.getProducts(low, high);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/category-counts")
    public ResponseEntity<List<CategoryCountProjection>> getCategoryCounts() {
        List<CategoryCountProjection> categoryCounts = productService.getCategoryCounts();
        return new ResponseEntity<>(categoryCounts, HttpStatus.OK);
    }
}
