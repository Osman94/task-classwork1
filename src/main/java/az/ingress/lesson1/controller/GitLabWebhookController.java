package az.ingress.lesson1.controller;

import az.ingress.lesson1.service.TelegramService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/webhook")
public class GitLabWebhookController {

    private final TelegramService telegramService;

    public GitLabWebhookController(TelegramService telegramService) {
        this.telegramService = telegramService;
    }

    @PostMapping
    public ResponseEntity<String> handleWebhook(@RequestBody Map<String, Object> payload) {
        if ("merge_request".equals(payload.get("object_kind"))) {
            Map<String, Object> mergeRequest = (Map<String, Object>) payload.get("object_attributes");
            String title = (String) mergeRequest.get("title");
            String url = (String) mergeRequest.get("url");
            String author = (String) ((Map<String, Object>) payload.get("last_commit")).get("author");

            // Construct the message
            String message = String.format(
                    "🔔 New Merge Request Created:\n\n*Title*: %s\n*Author*: %s\n[View Merge Request](%s)",
                    title, author, url
            );

            telegramService.sendMessage(message);
        }
        return ResponseEntity.ok("Webhook processed");
    }
}
