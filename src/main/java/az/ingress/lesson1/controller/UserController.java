package az.ingress.lesson1.controller;

import az.ingress.lesson1.model.dto.auth.CreateUserRequest;
import az.ingress.lesson1.model.dto.auth.LoginRequest;
import az.ingress.lesson1.model.dto.auth.RefreshTokenRequest;
import az.ingress.lesson1.model.response.LoginResponse;
import az.ingress.lesson1.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/create")
    public ResponseEntity<String> userCreate(@RequestBody @Valid CreateUserRequest createUserRequest) {
        userService.createUser(createUserRequest);
        return ResponseEntity.ok("User created");
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok(userService.login(loginRequest));
    }

    @PostMapping("/refresh-token")
    public ResponseEntity<LoginResponse> login(@RequestBody RefreshTokenRequest refreshTokenRequest) {
        return ResponseEntity.ok(userService.generateTokensWithGivenRefreshToken(refreshTokenRequest));
    }


}
