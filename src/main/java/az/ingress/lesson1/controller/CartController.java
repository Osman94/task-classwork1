package az.ingress.lesson1.controller;

import az.ingress.lesson1.model.dto.CartDto;
import az.ingress.lesson1.service.CartService;
import az.ingress.lesson1.model.projection.CartWithProductDetailsProjection;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shopping-carts")
@RequiredArgsConstructor
public class CartController {

    private final CartService cartService;

    @PostMapping
    public ResponseEntity<Void> createCart(@RequestBody CartDto cartDto) {
        cartService.createShoppingCart(cartDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/{id}/{productId}")
    public ResponseEntity<Void> addProductToCart(@PathVariable(name = "id") Long cartId, @PathVariable Long productId) {
        cartService.addProductToCart(cartId, productId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}/product/{productId}")
    public ResponseEntity<Void> deleteProductFromCart(@PathVariable(name = "id") Long cartId, @PathVariable Long productId) {
        cartService.deleteProductFromCart(cartId, productId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<CartWithProductDetailsProjection>> getCartWithProductDetails(@PathVariable Long id) {
        List<CartWithProductDetailsProjection> cartDetails = cartService.getCartAndProducts(id);
        return new ResponseEntity<>(cartDetails, HttpStatus.OK);
    }
}