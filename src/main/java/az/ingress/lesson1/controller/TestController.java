package az.ingress.lesson1.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class TestController {


    @GetMapping("/welcome")
    public String getWelcomeMessage() {
        String language = LocaleContextHolder.getLocale().getLanguage();
        return language.equals("en") ? "Welcome!" : "Localized Welcome Message";
    }

    @GetMapping("/user")
    public String userMethod() {
        return "Hello from user api";
    }

    @GetMapping("/admin")
    public String adminMethod() {
        return "Hello from admin api";
    }

    @GetMapping("/hello")
    public String hello(@RequestHeader(name = "Accept-Language") String language) {
        return "hello";
    }


}
