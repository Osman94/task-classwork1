package az.ingress.lesson1.repository;

import az.ingress.lesson1.model.entity.Cart;
import az.ingress.lesson1.model.projection.CartWithProductDetailsProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CartRepository extends JpaRepository<Cart, Long> {

    boolean existsByName(String name);

    boolean existsById(Long id);

    @Query(value = "SELECT p.name AS productName, p.price AS price, p.description AS description, c.name AS categoryName " +
            "FROM cart_products cp " +
            "JOIN product p ON cp.product_id = p.id " +
            "JOIN category c ON p.category_id = c.id " +
            "WHERE cp.cart_id = :cartId", nativeQuery = true)
    List<CartWithProductDetailsProjection> getCartAndProductsDetails(@Param("cartId") Long cartId);

}
