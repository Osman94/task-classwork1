package az.ingress.lesson1.repository;

import az.ingress.lesson1.model.projection.CategoryCountProjection;
import az.ingress.lesson1.model.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    boolean existsByName(String name);

    List<Product> findAllByPriceLessThanEqualAndPriceGreaterThanEqual(Integer low, Integer high);

    @Query(value = "SELECT category_id as category," +
            " COUNT(*) as productCount " +
            "FROM product " +
            "GROUP BY category_id", nativeQuery = true)
    List<CategoryCountProjection> findCategoryCounts();

}
