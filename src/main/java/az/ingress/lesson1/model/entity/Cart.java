package az.ingress.lesson1.model.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "shopping_carts")
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;

    @Builder.Default
    @ManyToMany(mappedBy = "carts", fetch = FetchType.LAZY)
    @ToString.Exclude
    List<Product> products = new ArrayList<>();
}

