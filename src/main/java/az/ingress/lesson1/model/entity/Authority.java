package az.ingress.lesson1.model.entity;

import jakarta.persistence.Embeddable;
import jakarta.websocket.server.ServerEndpoint;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Builder
public class Authority implements GrantedAuthority {

    String authority;
}
