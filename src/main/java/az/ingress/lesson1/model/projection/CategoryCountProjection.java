package az.ingress.lesson1.model.projection;

public interface CategoryCountProjection {
    String getCategory();
    int getProductCount();
}
