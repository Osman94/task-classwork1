package az.ingress.lesson1.model.projection;

public interface CartWithProductDetailsProjection {

    String getProductName();
    String getPrice();
    String getDescription();
    String getCategoryName();

}
