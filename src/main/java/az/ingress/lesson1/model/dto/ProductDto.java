package az.ingress.lesson1.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductDto {
    String name;
    Integer price;
    String description;

    @JsonProperty("productDetails")
    ProductDetailsDto productDetailsDto;

    CategoryDto category;
}
