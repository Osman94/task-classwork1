package az.ingress.lesson1.model.dto.auth;


import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateUserRequest {

    @NotBlank(message = "VALID_FULL_NAME_NOT_BLANK")
    @NotNull
    @Size(min = 3, max = 25, message = "VALID_FULL_NAME_SIZE")
    private String fullName;

    @NotBlank(message = "VALID_USERNAME_NOT_BLANK")
    @NotNull
    @Size(min = 3, max = 10, message = "USERNAME_SIZE")
    private String username;

    @Email(message = "VALID_EMAIL_ADDRESS")
    @NotNull
    @NotBlank(message = "VALID_EMAIL_NOT_BLANK")
    private String email;

    @Pattern(regexp = "\\d{3}-\\d{3}-\\d{2}-\\d{2}", message = "VALID_PHONE_NUMBER_FORMAT")
    private String phoneNumber;

    @Size(min = 8, message = "VALID_PASSWORD_SIZE")
    private String password;

    private String passwordConfirm;

    @AssertTrue(message = "VALID_PASSWORDS_MATCH")
    public boolean isPasswordsMatching() {
        return password != null && password.equals(passwordConfirm);
    }
}
