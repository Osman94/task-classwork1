package az.ingress.lesson1.model.response;


import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Builder
@Getter
public class LoginResponse {

    public String accessToken;
    public String refreshToken;
}
