package az.ingress.lesson1.model.response;

import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
@RequiredArgsConstructor
@Builder
public class ErrorResponse {

    private String code;
    private String message;
    private String details;
    private LocalDateTime timestamp;

}