package az.ingress.lesson1.service.impl;

import az.ingress.lesson1.model.dto.CartDto;
import az.ingress.lesson1.model.entity.Cart;
import az.ingress.lesson1.model.entity.Product;
import az.ingress.lesson1.model.projection.CartWithProductDetailsProjection;
import az.ingress.lesson1.repository.CartRepository;
import az.ingress.lesson1.repository.ProductRepository;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CartImplTest {

    @InjectMocks
    private CartImpl cartService;

    @Mock
    private CartRepository cartRepository;

    @Mock
    private ProductRepository productRepository;


    @Mock
    private RedisTemplate<Object, Object> redisTemplate;

    @Mock
    private ValueOperations<Object, Object> valueOperations;

    @Mock
    private ListOperations<Object, Object> listOperations;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(redisTemplate.opsForList()).thenReturn(listOperations);
    }

    @Test
    void givenCartIdWhenGetCartAndProductsThenSuccess() {
        Long cartId = 1L;
        String redisKey = "cartProducts:" + cartId;

        List<CartWithProductDetailsProjection> cartProductsFromDB = List.of();
        when(redisTemplate.opsForValue().get(redisKey)).thenReturn(null);
        when(cartRepository.existsById(cartId)).thenReturn(true);
        when(cartRepository.getCartAndProductsDetails(cartId)).thenReturn(cartProductsFromDB);

        List<CartWithProductDetailsProjection> result = cartService.getCartAndProducts(cartId);

        verify(cartRepository).getCartAndProductsDetails(cartId);
        verify(redisTemplate.opsForValue()).set(redisKey, cartProductsFromDB);
        assertEquals(cartProductsFromDB, result);
    }

    @Test
    void givenCartIdWhenCartDoesNotExistThenThrowException() {
        Long cartId = 1L;
        String redisKey = "cartProducts:" + cartId;

        when(redisTemplate.opsForValue().get(redisKey)).thenReturn(null);
        when(cartRepository.existsById(cartId)).thenReturn(false);

        assertThatThrownBy(() -> cartService.getCartAndProducts(cartId))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Cart doesn't exist");
    }
    @Test
    void givenCartAndProductWhenDeleteProductFromCartThenSuccess() {
        Long cartId = 1L;
        Long productId = 1L;

        Cart cart = new Cart();
        List<Cart> carts = new ArrayList<>();
        carts.add(cart);

        Product product = new Product();
        product.setCarts(carts);

        when(cartRepository.findById(cartId)).thenReturn(Optional.of(cart));
        when(productRepository.findById(Math.toIntExact(productId))).thenReturn(Optional.of(product));

        cartService.deleteProductFromCart(cartId, productId);

        verify(productRepository).save(product);
        verify(listOperations).remove("cartProducts:" + cartId, 1, product);
        verify(redisTemplate).delete("cart:" + cartId);
        assertFalse(product.getCarts().contains(cart));
    }

    @Test
    void givenNonExistentCartWhenDeleteProductFromCartThenThrowException() {
        // Arrange
        Long cartId = 1L;
        Long productId = 1L;

        when(cartRepository.findById(cartId)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> cartService.deleteProductFromCart(cartId, productId))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessage("Cart not found");

        verify(redisTemplate, never()).delete(anyString());
        verify(listOperations, never()).remove(anyString(), anyLong(), any());
    }

    @Test
    void givenNonExistentProductWhenDeleteProductFromCartThenThrowException() {

        Long cartId = 1L;
        Long productId = 1L;

        Cart cart = new Cart();
        when(cartRepository.findById(cartId)).thenReturn(Optional.of(cart));
        when(productRepository.findById(Math.toIntExact(productId))).thenReturn(Optional.empty());

        assertThatThrownBy(() -> cartService.deleteProductFromCart(cartId, productId))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessage("Product not found");

        verify(redisTemplate, never()).delete(anyString());
        verify(listOperations, never()).remove(anyString(), anyLong(), any());
    }


    @Test
    void givenCartDtoWhenCreateShoppingCartThenSuccess() {
        // Arrange
        CartDto cartDto = new CartDto();
        cartDto.setName("Test Cart");

        Cart cart = Cart.builder()
                .id(1L)
                .name(cartDto.getName())
                .build();

        when(cartRepository.existsByName(cartDto.getName())).thenReturn(false);
        when(cartRepository.save(any(Cart.class))).thenReturn(cart);

        // Act
        cartService.createShoppingCart(cartDto);

        // Assert
        var captor = ArgumentCaptor.forClass(Cart.class);
        verify(cartRepository, times(1)).save(captor.capture());

        Cart savedCart = captor.getValue();
        assertThat(savedCart.getId()).isEqualTo(cart.getId());
        assertThat(savedCart.getName()).isEqualTo(cart.getName());

        verify(redisTemplate.opsForValue()).set("cart:" + savedCart.getId(), savedCart);
    }



    @Test
    void givenExistingCartDtoWhenCreateShoppingCartThenThrowException() {
        // Arrange
        CartDto cartDto = new CartDto();
        cartDto.setName("Existing Cart");

        when(cartRepository.existsByName(cartDto.getName())).thenReturn(true);

        assertThatThrownBy(() -> cartService.createShoppingCart(cartDto))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Cart already exists");

        verify(cartRepository, never()).save(any(Cart.class));
        verify(redisTemplate, never()).opsForValue().set(anyString(), any());
    }


}