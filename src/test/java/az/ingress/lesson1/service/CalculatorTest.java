package az.ingress.lesson1.service;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
class CalculatorTest {

    @InjectMocks
    private Calculator calculator;

    @Test
    void givenTwoNumbersWhenSumThenSuccess() {
        var result = calculator.add(10, 15);

        assertThat(result).isEqualTo(25);
    }

    @Test
    void givenTwoNumbersWhenDivideThenSuccess() {

        assertThat(calculator.divide(20, 4)).isEqualTo(5);
    }

    @Test
    void givenTwoNumbersWhenDivideToZeroThenException() {
        assertThatThrownBy(() -> calculator.divide(10,0))
                .isInstanceOf(ArithmeticException.class)
                .hasMessage("Divide by zero prohibited");
    }

}